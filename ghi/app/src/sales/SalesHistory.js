import React, { useEffect, useState } from 'react';

function SalesHistory () {

  const [salespeople, setSalespeople] = useState([]);
  const fetchSalespeopleData = async () => {
		const url = "http://localhost:8090/api/salespeople/"

		const response = await fetch(url);

		if (response.ok) {
				const data = await response.json();
				setSalespeople(data.salespeople);
		}
	}

  const [sales, setSales] = useState([]);
  const fetchSalesData = async () => {
		const url = "http://localhost:8090/api/sales/"

		const response = await fetch(url);

		if (response.ok) {
				const data = await response.json();
				setSales(data.sales);
		}
	}

  useEffect(() => {
    fetchSalespeopleData()
  }, []);

  useEffect(() => {
    fetchSalesData()
  }, []);

  const [salesperson, setSalesperson] = useState('');
	const handleSalespersonChange = (event) => {
		const value = event.target.value;
		setSalesperson(value);
    fetchSalespeopleData();
	}
  const getFilteredSales = (salesperson, sales) => {
    if (!salesperson) {
      return sales
    }
    return sales.filter(sale => {
      for (const [key, value] of Object.entries(sale)) {
        if (sale.salesperson.employee_id.includes(salesperson)) {
          return sale.salesperson.employee_id.includes(salesperson)
        }
      }
    })
  }

  const filteredSales = getFilteredSales(salesperson, sales)

  return (
    <div>
      <div className="my-5 container">
        <div className="form-floating mb-3">
          <select
          onChange={handleSalespersonChange}
          value={salesperson}
          required
          name="salesperson"
          id="salesperson"
          className="form-select"
          >
            <option
            value=""
            onChange={e => setSalesperson(e.target.value)}
            >
              Choose a salesperson
            </option>
              {salespeople.map(salesperson => {
                return (
                  <option key={salesperson.id} value={salesperson.employee_id}>
                    {salesperson.first_name} {salesperson.last_name} - {salesperson.employee_id}
                  </option>
                );
              })}
          </select>
          <div>
            <table className="table table-striped mt-3">
              <thead>
                <tr>
                  <th>Salesperson</th>
                  <th>Customer</th>
                  <th>Automobile VIN</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                {filteredSales.map((sale) => {
                  return (
                    <tr key={sale.href}>
                      <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                      <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                      <td>{sale.automobile.vin}</td>
                      <td>${sale.price}</td>
                    </tr>
                  )
                })
                }
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  )
};

export default SalesHistory;
