import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

function SalespeopleForm () {
  const [firstname, setFirstname] = useState('');
  const handleFirstnameChange = (event) => {
    const value = event.target.value;
    setFirstname(value);
  }

  const [lastname, setLastname] = useState('');
  const handleLastnameChange = (event) => {
    const value = event.target.value;
    setLastname(value);
  }

  const [employeeid, setEmployeeid] = useState('');
  const handleEmployeeidChange = (event) => {
    const value = event.target.value;
    setEmployeeid(value);
  }

  const navigateTo = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.first_name = firstname;
    data.last_name = lastname;
    data.employee_id = employeeid;

    const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-type': 'application/json'
      }
    }

    const response = await fetch(salespeopleUrl, fetchConfig);
    if (response.ok) {
      navigateTo('/salespeople')
    }
  }

  return (
    <div>
      <div className="my-2 container">
        <Link to={'..'}>
          <button className="btn bg-2" id="Button">BACK TO LIST</button>
        </Link>
      </div>

      <div className="shadow p-4 mt-4">
        <h1>Register Salesperson</h1>
        <form onSubmit={handleSubmit} >
          <div className="form-floating mb-3">
            <input onChange={handleFirstnameChange} placeholder='first name' type='text' value={firstname} className="form-control"/>
            <label htmlFor='firstname'>First Name</label>
          </div>

          <div className="form-floating mb-3">
            <input onChange={handleLastnameChange} placeholder='last name' type='text' value={lastname} className="form-control"/>
            <label htmlFor='lastname'>Last Name</label>
          </div>

          <div className="form-floating mb-3">
            <input onChange={handleEmployeeidChange} placeholder='employee id' type='text' value={employeeid} className="form-control"/>
            <label htmlFor='employee id'>Employee ID</label>
          </div>

          <button className="btn bg-2" id="Button">Register</button>
        </form>
      </div>

    </div>
  )
}

export default SalespeopleForm;
