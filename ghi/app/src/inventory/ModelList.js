import React, { useEffect, useState } from 'react';

export default function ModelList() {
    const [vehicles, setVehicles] = useState([])
    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/models/');
        const data = await response.json();
        setVehicles(data.models)
    }
    useEffect(() => {
        getData()
    }, [])
    return (
        <div>
            <h1>Vehicles:</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {vehicles.map(vehicles => {
                        return (
                            <tr key={vehicles.id}>
                                <td>{vehicles.name}</td>
                                <td>{vehicles.manufacturer.name}</td>
                                <td><img src={vehicles.picture_url}
                                alt="car-pic"
                                width="320"
                                height="200" /></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}
