import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturesForm from './inventory/ManufacturersForm';
import ManufacturesList from './inventory/ManufacturersList';
import ModelForm from './inventory/ModelForm';
import ModelList from './inventory/ModelList';
import AutomobileForm from './inventory/AutomobileForm';
import AutomobileList from './inventory/AutomobileList';
import SalespeopleList from './sales/SalespeopleList';
import SalespeopleForm from './sales/SalespeopleForm';
import SalesList from './sales/SalesList';
import SalesForm from './sales/SalesForm';
import SalesHistory from './sales/SalesHistory';
import CustomerList from './sales/CustomerList';
import CustomerForm from './sales/CustomerForm';
import TechicianForm from './Service/TechnicianForm';
import TechicianList from './Service/TechnicianList';
import AppointmentList from './Service/AppointmentList';
import AppointmentForm from './Service/AppointmentForm';
import AppointmentHistory from './Service/AppointmentHistory';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="automobiles">
            <Route index element={<AutomobileList/>} />
            <Route path="create" element={<AutomobileForm/>} />
          </Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturesList />} />
            <Route path="create" element={<ManufacturesForm />} />
          </Route>
          <Route path="models">
            <Route index element={<ModelList/>} />
            <Route path="create" element={<ModelForm />} />
          </Route>
          <Route path="customers">
            <Route path="" element={<CustomerList />} />
            <Route path="create" element={<CustomerForm />} />
          </Route>
          <Route path="salespeople">
            <Route path="" element={<SalespeopleList />} />
            <Route path="create" element={<SalespeopleForm />} />
          </Route>
          <Route path="sales">
            <Route path="" element={<SalesList />} />
            <Route path="create" element={<SalesForm />} />
            <Route path="history" element={<SalesHistory />} />
          </Route>
          <Route path="technician">
            <Route path="" element={<TechicianList />} />
            <Route path="create" element={<TechicianForm/>} />
          </Route>
          <Route path="appointment">
            <Route path="" element={<AppointmentList />} />
            <Route path="create" element={<AppointmentForm/>} />
            <Route path="history" element={<AppointmentHistory/>} />
          </Route>


        </Routes>
      </div>
    </BrowserRouter>
  );
}


export default App;
