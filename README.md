# CarCar
CarCar is an application for managing the aspects of an automobile dealership. It manages the inventory, automobile sales, and automobile services.

### Team 33:
* David Sanchez - Service
* Kenny Phung - Sales

## How to Install/Run CarCar
**[REQUIRED]:** Ensure that the **latest versions** of **Docker, Git, and Node.js** are **installed** before starting.

<details>

<summary>Part A: Cloning/Downloading CarCar</summary>

1. Fork this repository
2. Clone the forked repository onto your local computer:<br>
    `git clone <<respository.url.here>>`

</details>

<br>

<details>

<summary>Part B: Build and Run CarCar App</summary>

1. cd in the project folder after cloning:
    ```
    cd ./project-beta/
    ````

2. Build and run the project using Docker with these commands:
    ```
    docker volume create beta-data
    docker-compose build
    docker-compose up
    ```
3. Check `project-beta-react-1` container logs in Docker, wait for dev server to deploy. Successful log looks like this example:
    ![ALT](./images/ServerStart.jpg)

4. View the project in the browser: http://localhost:3000/

    ![ALT](./images/CarCarWebsite.jpg)

</details>


## Diagram
![ALT](./images/Diagram.jpg)

## API Documentation

### Inventory API
<details>

 <summary>Manufacturers URLs</summary>

 | Action | Method | URL
 | ----------- | ----------- | ----------- |
 | List manufacturers | GET | http://localhost:8100/api/manufacturers/
 | Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
 | Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/id/
 | Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/id/
 | Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/

 </details>

<details>

<summary>Vehicle Models URLs</summary>

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List vehicle models | GET | http://localhost:8100/api/models/
| Create a vehicle model | POST | http://localhost:8100/api/models/
| Get a specific vehicle model | GET | http://localhost:8100/api/models/id/
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/id/
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/id/

</details>

<details>

<summary>Automobiles URLs</summary>

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/vin/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/vin/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/vin/

</details>

### Service API
<details>

<summary> Models </summary>

The Service microservice is comprised of 3 models and the AutomobileVO is polled from the Inventory microservice:
- Technician
    - first_name
    - last_name
    - employee_id
- Appointment
    - date_time
    - reason
    - status
    - vin
    - customer
    - technician (foreign key)
    - vip
- AutomobileVO
    - vin
    - sold
</details>

<details>
<summary>URLs/Routes</summary>

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/technicians/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Delete a specific technician | DELETE | http://localhost:8080/api/technicians/:id/
| List appointments | GET | http://localhost:8080/api/appointments/
| Create an appointment | POST | http://localhost:8080/api/appointments/
| Delete an appointment | DELETE | http://localhost:8080/api/appointments/:id/
| Set appointment status to "canceled" | PUT | http://localhost:8080/api/appointments/:id/cancel/
| Set appointment status to "finished" | PUT | 	http://localhost:8080/api/appointments/:id/finish/
</details>

<!-- Technicians collapsible START  -->
<details>
<summary> Accessing Endpoints: Technicians </summary>

<!-- NESTED COLLAPSIBLES START  -->
<details>
<summary> List technicians (GET) </summary>

1. Create a GET **request** @ http://localhost:8080/api/technicians/

2. Sample GET **response** (JSON):
```json
{
	"technicians": [
		{
			"href": "/api/technicians/1/",
			"first_name": "David",
			"last_name": "Sanchez",
			"employee_id": "112233",
			"id": 1
		}
	]
}
```
</details>

<details>
<summary> Create a technician (POST) </summary>

1. Create a POST **request** @ http://localhost:8080/api/technicians/

2. Sample POST **request** (JSON):
```json
{
  "first_name": "David",
  "last_name": "Sanchez",
  "employee_id": "112233"
}
```

3. Sample POST **response**
```json
{
	"href": "/api/technicians/1/",
	"first_name": "David",
	"last_name": "Sanchez",
	"employee_id": "112233",
	"id": 1
}
```

</details>

<details>
<summary> Delete a technician (DELETE) </summary>

1. Create a DELETE **request** @ http://localhost:8080/api/technicians/1/

2. Sample DELETE **response** (JSON):
```json
{
	"deleted": true
}
```
</details>
<!-- NESTED COLLAPSIBLES END  -->

</details>
<!-- Technicians collapsible END  -->

<!-- Appointments collapsible START  -->
<details>
<summary> Accessing Endpoints: Appointments </summary>

<details>
<summary> List Appointments (GET) </summary>

1. Create a GET **request** @ http://localhost:8080/api/appointments/

2. Sample GET **response** (JSON):
```json
{
	"appointments": [
		{
			"href": "/api/appointments/1/",
			"date_time": "2023-06-08T17:57:00+00:00",
			"reason": "oil change",
			"status": "canceled",
			"vin": "121234123432123",
			"customer": "David",
			"technician": {
				"href": "/api/technicians/11/",
				"first_name": "Nicky",
				"last_name": "Porter",
				"employee_id": "1237654",
				"id": 11
			},
			"id": 30,
			"vip": false
		}
	]
}

```
</details>

<details>
<summary> Create an appointment (POST) </summary>

1. Create a POST **request** @ http://localhost:8080/api/appointments/

2. Sample POST **request** (JSON):
```json
{
	"date_time": "2023-06-08",
    "reason": "over heat",
    "status": "canceled",
    "vin": "1234567891",
	"customer": "David Sanchez",
	"technician": "11"
}
```

3. Sample POST **response**
```json
{
	"href": "/api/appointments/38/",
	"date_time": "2023-06-08",
	"reason": "over het",
	"status": "canceled",
	"vin": "1234567891",
	"customer": "David Sanchez",
	"technician": {
		"href": "/api/technicians/11/",
		"first_name": "Nicky",
		"last_name": "Porter",
		"employee_id": "1237654",
		"id": 11
	},
	"id": 38,
	"vip": false
}
```
</details>

<details>
<summary> Delete a specific appointment (DELETE) </summary>

1. Create a DELETE **request** @ http://localhost:8080/api/appointments/11/

2. Sample DELETE **response**
```json
{
	"date_time": "2023-06-08T00:00:00+00:00",
	"reason": "oil change",
	"status": "canceled",
	"vin": "1234567891",
	"customer": "David Sanchez",
	"technician": {
		"href": "/api/technicians/1/",
		"first_name": "Nicky",
		"last_name": "Porter",
		"employee_id": "11127wer21",
		"id": 1
	},
	"id": null,
	"vip": false
}
```
</details>

<details>
<summary> Set appointment status to "canceled" (PUT) </summary>

1. Create a PUT **request** @ http://localhost:8080/api/appointments/19/cancel/

2. Sample PUT **response**
```json
{
	"href": "/api/appointments/19/",
	"date_time": "2023-06-08T00:00:00+00:00",
	"reason": "over heat",
	"status": "canceled",
	"vin": "1234567891",
	"customer": "David Sanchez",
	"technician": {
		"href": "/api/technicians/1/",
		"first_name": "Nicky",
		"last_name": "Porter",
		"employee_id": "11127wer21",
		"id": 1
	},
	"id": 19,
	"vip": false
}
```
</details>

<details>
<summary> Set appointment status to "finished" (PUT) </summary>

1. Create a PUT **request** @ http://localhost:8080/api/appointments/4/finish/

2. Sample PUT **response**
```json
{
	"href": "/api/appointments/4/",
	"date_time": "2023-06-07T00:00:00+00:00",
	"reason": "broken",
	"status": "finished",
	"vin": "1234567891",
	"customer": "David Sanchez",
	"technician": {
		"href": "/api/technicians/4/",
		"first_name": "Nicky",
		"last_name": "Porter",
		"employee_id": "111j7w",
		"id": 4
	},
	"id": 4,
	"vip": false
}
```
</details>
<!-- NESTED COLLAPSIBLES END  -->

</details>
<!-- Appointments collapsible END  -->

</details>

​
### Sales API

<details>

<summary> Models </summary>

The Sales microservice is comprised of 3 models and the AutomobileVO is polled from the Inventory microservice:
- Salesperson
    - first_name
    - last_name
    - employee_id
- Customer
    - first_name
    - last_name
    - address
    - phone number
- Sale
    - automobile (foreign key)
    - salesperson (foreign key)
    - customer (foreign key)
    - price

- AutomobileVO (project-beta-inventory-api:8000/api/automobiles)
    - href
    - sold
    - VIN

</details>

<details>
<summary>URLs/Routes</summary>

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | http://localhost:8090/api/salespeople
| Create a salesperson | POST | http://localhost:8090/api/salespeople
| Update a salesperson | PUT | http://localhost:8090/api/salespeople/id
| Delete a specific salesperson | DELETE | http://localhost:8090/api/salespeople/id
| List customers | GET | http://localhost:8090/api/customers
| Create a customer | POST | http://localhost:8090/api/customers
| Update a specific customer | PUT | http://localhost:8090/api/customers/id
| Delete a specific customer | DELETE | http://localhost:8090/api/customers/id
| List sales | GET | http://localhost:8090/api/sales/
| Create a sale | POST | http://localhost:8090/api/sales/
| Update a sale | PUT | http://localhost:8090/api/sales/id
| Delete a sale | DELETE | http://localhost:8090/api/sales/id
</details>

<!-- Salesperson collapsible START  -->
<details>
<summary> Accessing Endpoints: Salesperson </summary>

<!-- NESTED COLLAPSIBLES START  -->
<details>
<summary> List Salespeople (GET) </summary>

1. Create a GET **request** @ http://localhost:8090/api/salespeople/

2. Sample GET **response** (JSON):
```json
{
	"salespeople": [
		{
			"href": "/api/salespeople/1/",
			"id": 1,
			"first_name": "Kenny",
			"last_name": "Phung",
			"employee_id": "888"
		}
	]
}
```
</details>

<details>
<summary> Create a salesperson (POST) </summary>

1. Create a POST **request** @ http://localhost:8090/api/salespeople/

2. Sample POST **request** (JSON):
```json
{
  "first_name": "Kenny",
  "last_name": "Phung",
  "employee_id": "888"
}
```

3. Sample POST **response**
```json
{
	"href": "/api/salespeople/1/",
	"id": 1,
	"first_name": "Kenny",
	"last_name": "Phung",
	"employee_id": "888"
}
```

</details>

<details>
<summary> Delete a salesperson (DELETE) </summary>

1. Create a DELETE **request** @ http://localhost:8090/api/salespeople/id

2. Sample DELETE **response** (JSON):
```json
{
	"deleted": true,
	"breakdown": {
		"sales_rest.Salesperson": 1
	}
}
```
</details>
<!-- NESTED COLLAPSIBLES END  -->

</details>
<!-- Salesperson collapsible END  -->

<!-- Customers collapsible START  -->
<details>
<summary> Accessing Endpoints: Customers </summary>

<details>
<summary> List Customers (GET) </summary>

1. Create a GET **request** @ http://localhost:8090/api/customers/

2. Sample GET **response** (JSON):
```json
{
	"customers": [
		{
			"href": "/api/customers/1/",
			"id": 1,
			"first_name": "Kenny",
			"last_name": "Phung",
			"address": "123 Fake St. Los Angeles, CA 99999",
			"phone_number": "1234567890"
		}
	]
}

```
</details>

<details>
<summary> Create a customer (POST) </summary>

1. Create a POST **request** @ http://localhost:8090/api/customers/

2. Sample POST **request** (JSON):
```json
{
    "first_name": "Kenny",
    "last_name": "Phung",
    "address": "123 Fake St. Los Angeles, CA 99999",
	"phone_number": "1234567890"
}
```

3. Sample POST **response**
```json
{
    "href": "/api/customers/1",
    "id": 1,
	"first_name": "Kenny",
	"last_name": "Phung",
	"address": "123 Fake St. Los Angeles, CA 99999",
	"phone_number": "1234567890"
}
```
</details>

<details>
<summary> Delete a customer (DELETE) </summary>

1. Create a DELETE **request** @ http://localhost:8090/api/customers/id/

2. Sample DELETE **response**
```json
{
	"deleted": true,
	"breakdown": {
		"sales_rest.Customer": 1
	}
}
```

</details>
<!-- NESTED COLLAPSIBLES END  -->

</details>
<!-- Customers collapsible END  -->

<!-- Sales collapsible START  -->
<details>
<summary> Accessing Endpoints: Sales </summary>

<!-- NESTED COLLAPSIBLES START  -->
<details>
<summary> List Sales (GET) </summary>

1. Create a GET **request** @ http://localhost:8090/api/sales/

2. Sample GET **response**
```json
{
	"sales": [
		{
			"href": "/api/sales/1/",
			"price": 88888,
			"automobile": {
				"vin": "111222333444",
				"import_href": "/api/automobiles/111222333444/"
			},
			"salesperson": {
				"href": "/api/salespeople/1/",
				"id": 1,
				"first_name": "Kenny",
				"last_name": "Phung",
				"employee_id": "888"
			},
			"customer": {
				"href": "/api/customers/2/",
				"id": 2,
				"first_name": "David",
				"last_name": "Sanchez",
				"address": "123 Fake St. Los Angeles, CA 99999",
				"phone_number": "1211211211"
			}
		}
	]
}
```
</details>

<details>
<summary> Create a Sale (POST) </summary>

1. Create a POST **request** @ http://localhost:8090/api/sales/

2. Sample POST **request**
```json
{
    "automobile": "/api/automobiles/111222333444/",
    "salesperson": 1,
    "customer": 2,
    "price": 88888
}
```

3. Sample POST **response**
```json
{
	"href": "/api/sales/2/",
	"price": 88888,
	"automobile": {
		"vin": "111222333444",
		"import_href": "/api/automobiles/111222333444/"
	},
	"salesperson": {
		"href": "/api/salespeople/1/",
		"id": 1,
		"first_name": "Kenny",
		"last_name": "Phung",
		"employee_id": "888"
	},
	"customer": {
		"href": "/api/customers/1/",
		"id": 1,
		"first_name": "Kenny",
		"last_name": "Phung",
		"address": "12345 Real St. Los Angeles, CA",
		"phone_number": "121-121-1212"
	}
}
```
</details>

<details>
<summary> Delete a Sale (DELETE) </summary>

1. Create a DELETE **request** @ http://localhost:8090/api/sales/1/

2. Sample DELETE **response**
```json
{
	"deleted": true,
	"breakdown": {
		"sales_rest.Sale": 1
	}
}
```

</details>
<!-- NESTED COLLAPSIBLES END  -->

</details>
<!-- Sales collapsible END  -->
